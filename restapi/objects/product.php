<?php
class Product
{
// database connection and table name
    private $conn;
    private $table_list = "scandiweb.list_product";
    private $table_prd_type = "scandiweb.product";
// object properties
    public $id_list_product;
    public $sku;
    public $name;
    public $price;
    public $desc;
    public $id_product;
    public $type;

    public function __construct($db)
    {
        $this->conn = $db;

    }

    public function read()
    {
        $query = "SELECT * FROM " . $this->table_list;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function delete()
    {
        $query = "DELETE FROM " . $this->table_list . " WHERE  id_list_product= :id_list_product";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":id_list_product", $this->id_list_product, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt;
    }

    public function create()
    {
        $query = "INSERT INTO `scandiweb`.`list_product` (`id_product`, `sku`, `name`, `price`, `desc`)
        VALUES (? ,? ,? ,? ,?)";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id_product);
        $stmt->bindParam(2, $this->sku);
        $stmt->bindParam(3, $this->name);
        $stmt->bindParam(4, $this->price);
        $stmt->bindParam(5, $this->desc);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function readType()
    {
        $query = "SELECT * FROM " . $this->table_prd_type;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

}
