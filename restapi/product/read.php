<?php

include_once '../config/cors.php';
include_once '../objects/product.php';
include_once '../config/database.php';

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);

$stmt = $product->read();
$num = $stmt->rowCount();

if ($num > 0) {

    $products_arr = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $description = $row['desc'];
        $str_arr = explode(",", $description);
        if ($row['id_product'] == '1') {
            $row['desc'] = "Size : " . $str_arr[0] . " MB";
        } elseif ($row['id_product'] == '2') {
            $row['desc'] = "Weight : " . $str_arr[0] . " KG";
        } elseif ($row['id_product'] == '3') {
            $row['desc'] = "Description : " . $str_arr[0] . " x " . $str_arr[1] . " x " . $str_arr[2];
        }

        extract($row);
        $product_item = array(
            "id_list_product" => $id_list_product,
            "id_product" => $id_product,
            "sku" => $sku,
            "name" => $name,
            "price" => $price,
            "desc" => $desc,

        );

        array_push($products_arr, $product_item);
    }

    http_response_code(200);
    echo json_encode($products_arr);
} else {

    http_response_code(404);

    echo json_encode(
        array("message" => "No products found.")
    );
}
