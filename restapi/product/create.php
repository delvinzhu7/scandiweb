<?php

include_once '../config/cors.php';
include_once '../config/database.php';
include_once '../objects/product.php';

$database = new Database();
$db = $database->getConnection();
$product = new Product($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->id_product) && !empty($data->sku)
    && !empty($data->name) && !empty($data->price)
    && !empty($data->desc1)
) {

    $desc = $data->desc1 . ',' . $data->desc2 . ',' . $data->desc3;

    $product->id_product = $data->id_product;
    $product->sku = $data->sku;
    $product->name = $data->name;
    $product->price = $data->price;
    $product->desc = $desc;

    if ($product->create()) {
        http_response_code(201);
        echo json_encode(array("message" => "Product was created."));
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to create product."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to create product. Data is incomplete."));
}
