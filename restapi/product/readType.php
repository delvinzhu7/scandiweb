<?php

include_once '../config/cors.php';
include_once '../objects/product.php';
include_once '../config/database.php';

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);

$stmt = $product->readType();
$num = $stmt->rowCount();

if ($num > 0) {
    $products_arr = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $product_item = array(
            "id_product" => $id_product,
            "type" => $type,

        );

        array_push($products_arr, $product_item);
    }
    http_response_code(200);
    echo json_encode($products_arr);
} else {
    http_response_code(404);
    echo json_encode(
        array("message" => "products type not found.")
    );
}
