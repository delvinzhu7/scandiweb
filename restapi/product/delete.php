<?php

include_once '../config/cors.php';
include_once '../config/database.php';
include_once '../objects/product.php';

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);

$product->id_list_product = isset($_GET['id_list_product']) ? $_GET['id_list_product'] : die();

$product->delete();

if ($product->delete()) {
    http_response_code(200);
} else {

    http_response_code(404);

    echo json_encode(array("message" => "Product does not exist."));
}
