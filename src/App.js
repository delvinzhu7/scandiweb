import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Routes } from 'react-router-dom';
import AddProduct from './Pages/AddProduct';
import Home from './Pages/Home';

function App() {
	return (
		<div>
			<main>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/addproduct" element={<AddProduct />} />
				</Routes>
			</main>
		</div>
	);
}

export default App;
