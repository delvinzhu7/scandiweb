import 'bootstrap/dist/css/bootstrap.min.css';
import './Home.css';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Stack from 'react-bootstrap/Stack';
import Container from 'react-bootstrap/esm/Container';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import axios from '../Auth/axios';
import request from '../Auth/Request';

export default function Home() {
	const [posts, setPosts] = useState([]);
	const [didDelete, DeleteItem] = useState(false);
	const { handleSubmit } = useForm({
		mode: 'onChange',
	});

	async function fetchData() {
		await axios
			.get(request.fetchLstPrd)
			.then((res) => {
				setPosts(res.data);
			})

			.catch((err) => {
				console.log(err);
			});
	}

	useEffect(() => {
		fetchData();
	}, []);

	const handleChange = (id) => {
		axios
			.delete(request.deletLstPrd + id)
			.then((res) => {
				console.log(res);
				fetchData();
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const onClickDelete = async () => {
		DeleteItem(!didDelete);
	};

	return (
		<>
			<Navbar>
				<Container>
					<Navbar.Brand href="/">Product List</Navbar.Brand>
				</Container>

				<Stack direction="horizontal" gap={3} className="stkBtn ">
					<Link to="/addproduct" className="btn btn-secondary ">
						ADD
					</Link>
					<div className="vr" />
					<Button
						className="stkBtn1"
						variant="secondary"
						onClick={handleSubmit(onClickDelete)}
					>
						MASS DELETE
					</Button>
				</Stack>
			</Navbar>
			<Container>
				<div className="cardStyle">
					{posts.map((posts) => {
						return (
							<Card className="cardPad" key={posts.id_list_product}>
								<Form.Check
									type="checkbox"
									className="delete-checkbox"
									onChange={() => handleChange(posts.id_list_product)}
									value={posts.id_list_product}
									disabled={didDelete === false}
								/>
								<Card.Body>
									<Card.Title className="text-center">{posts.sku}</Card.Title>
									<Card.Text className="text-center">{posts.name}</Card.Text>
									<Card.Text className="text-center">
										{'$ ' + posts.price}
									</Card.Text>
									<Card.Text className="text-center">{posts.desc}</Card.Text>
								</Card.Body>
							</Card>
						);
					})}
				</div>
			</Container>
		</>
	);
}
