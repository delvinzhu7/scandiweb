import 'bootstrap/dist/css/bootstrap.min.css';
import './AddProduct.css';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Stack from 'react-bootstrap/Stack';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Link, Navigate } from 'react-router-dom';
import axios from '../Auth/axios';
import request from '../Auth/Request';

function AddProduct() {
	const [getOpt, setOpt] = useState('');
	const [posts, setPosts] = useState([]);
	const [didData, setData] = useState(false);
	const [chgOpt, setChgOpt] = useState(false);
	const [number, setNumber] = useState('');
	const [error, setError] = useState('');
	const {
		register,
		handleSubmit,
		resetField,
		clearErrors,
		formState: { errors },
	} = useForm({
		mode: 'onChange',
		defaultValues: {
			desc1: '',
			desc2: '',
			desc3: '',
		},
	});
	const onSubmit = async (data) => {
		await axios
			.post(request.addLstPrd, data)
			.then((res) => {
				console.log(res);
				setData(true);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	useEffect(() => {
		async function fetchData() {
			await axios
				.get(request.fetchPrd)
				.then((res) => {
					setPosts(res.data);
				})
				.catch((err) => {
					console.log(err);
				});
		}
		fetchData();
	}, []);

	function handleOnchangeOpt(e) {
		const selectedOpt = e.target.value;
		setChgOpt(true);
		if (selectedOpt !== '') {
			clearErrors('id_product');
			setOpt(selectedOpt);
			resetField('desc1');
			resetField('desc2');
			resetField('desc3');
		}
	}

	const numberHandleChange = (e) => {
		e.preventDefault();
		const re = /^[0-9\b]+$/;

		if (e.target.value === '' || re.test(e.target.value)) {
			setNumber(e.target.value);
			setError('');
			console.log(e.target.value);
		} else {
			setError('❌ Only accept number characters between 0-9');
		}
	};

	return (
		<>
			<Navbar>
				<Container>
					<Navbar.Brand href="/">Product List</Navbar.Brand>
				</Container>
				<Stack direction="horizontal" gap={3} className="stkBtn">
					<Button variant="secondary" onClick={handleSubmit(onSubmit)}>
						Save
					</Button>
					<div className="vr" />
					<Link to="/" className="btn btn-secondary">
						Cancel
					</Link>
				</Stack>
			</Navbar>
			{didData ? (
				<Navigate to="/" />
			) : (
				<main>
					<Container>
						<Form className="form">
							<Form.Group className="mb-3">
								<div className="label">
									<Form.Label className="labelItem">SKU</Form.Label>
									<Form.Control
										{...register('sku', { required: true })}
										type="text"
										placeholder="Input Sku"
										id="sku"
									/>
									{errors.sku && (
										<p className="palert">This field is Required</p>
									)}
								</div>

								<div className="label">
									<Form.Label className="labelItem">Name</Form.Label>
									<Form.Control
										type="text"
										placeholder="Input Name"
										{...register('name', { required: true })}
										id="name"
									/>
									{errors.name && (
										<p className="palert">This field is Required</p>
									)}
								</div>

								<div className="label">
									<Form.Label className="labelItem">Price</Form.Label>
									<Form.Control
										onInput={numberHandleChange}
										value={number}
										placeholder="Input Price"
										{...register('price', { required: true })}
										id="price"
									/>
									{errors.price && (
										<p className="palert">This field is Required</p>
									)}
								</div>
								{error && <span className="isNumber">{error}</span>}
							</Form.Group>
							<div className="label">
								<label className="labelItem">Type Switcher</label>
								<Form.Select
									id="productType"
									{...register('id_product', { required: true })}
									onChange={(e) => handleOnchangeOpt(e)}
								>
									<option value="" disabled={chgOpt}>
										Select Type
									</option>
									{posts.map((post) => (
										<option key={post.id_product} value={post.id_product}>
											{post.type}
										</option>
									))}
								</Form.Select>
								{errors.id_product && (
									<p className="palert">This field is Required</p>
								)}
							</div>

							<div className="SwitchForm">
								{(() => {
									switch (getOpt) {
										case '1':
											return (
												<Form.Group className="mb-3">
													<div className="label">
														<Form.Label className="labelItem">
															Size (MB)
														</Form.Label>
														<Form.Control
															type="number"
															placeholder="Input Size"
															id="size"
															{...register('desc1', { required: true })}
														/>
														{errors.desc1 && (
															<p className="palert">This field is Required</p>
														)}
													</div>

													<Form.Text className="text-muted">
														Please, provide dimensions.
													</Form.Text>
												</Form.Group>
											);
										case '2':
											return (
												<Form.Group className="mb-3">
													<div className="label">
														<Form.Label className="labelItem">
															Weigth (KG)
														</Form.Label>
														<Form.Control
															type="number"
															placeholder="Input weigth"
															id="weigth"
															{...register('desc1', { required: true })}
														/>
														{errors.desc1 && (
															<p className="palert">This field is Required</p>
														)}
													</div>

													<Form.Text className="text-muted">
														Please, provide weight.
													</Form.Text>
												</Form.Group>
											);
										case '3':
											return (
												<Form.Group className="mb-3">
													<div className="label">
														<Form.Label className="labelItem">
															Height (CM)
														</Form.Label>
														<Form.Control
															placeholder="Input Height"
															id="height"
															type="number"
															{...register('desc1', { required: true })}
														/>
														{errors.desc1 && (
															<p className="palert">This field is Required</p>
														)}
													</div>

													<div className="label">
														<Form.Label className="labelItem">
															Width (CM)
														</Form.Label>
														<Form.Control
															placeholder="Input Width"
															id="width"
															type="number"
															{...register('desc2', { required: true })}
														/>
														{errors.desc2 && (
															<p className="palert">This field is Required</p>
														)}
													</div>

													<div className="label">
														<Form.Label className="labelItem">
															Length (CM)
														</Form.Label>
														<Form.Control
															placeholder="Input Length"
															id="length"
															type="number"
															{...register('desc3', { required: true })}
														/>
														{errors.desc3 && (
															<p className="palert">This field is Required</p>
														)}
													</div>

													<Form.Text className="text-muted">
														Please, provide size.
													</Form.Text>
													<div></div>
												</Form.Group>
											);
										default:
											return '';
									}
								})()}
							</div>
						</Form>
					</Container>
				</main>
			)}
		</>
	);
}
export default AddProduct;
