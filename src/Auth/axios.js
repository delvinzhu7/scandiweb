import axios from 'axios';

const instance = axios.create({
	// baseURL: 'http://localhost/Scandiapi',
	baseURL: 'http://localhost/restapi',
});

export default instance;
