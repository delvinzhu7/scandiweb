const request = {
	fetchPrd: `/product/readType.php`,
	fetchLstPrd: `/product/read.php`,
	deletLstPrd: `/product/delete.php?id_list_product=`,
	addLstPrd: `/product/create.php`,
};

export default request;
